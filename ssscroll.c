// sakura's simple scroll

#include <stdlib.h> // EXIT_FAILURE, EXIT_SUCCESS
#include <stdio.h>
#include <unistd.h> // read
#include <string.h>

#include "utf8.h"

#define VERSION "1.0"
#define MAX_LEN 512

int mk_wcwidth(wchar_t ucs);

static void usage(void)
{
	fputs("ssscroll " VERSION
	      "\nusage: ssscroll [-a afix] [-p prefix] [-s separator] [-1 index] [-t truncate_size] [-i text] [-h] [-]\n"
	      "if input is received from stdin, a '-' option must be provided.\n"
	      "output must fit into 512 bytes, can be increased by editing MAX_LEN macro",
	      stderr);
}

// returns the amount of bytes the next n utf8 codepoints take
static int utf8chrlen(const utf8_int8_t *str, size_t n)
{
	size_t length = 0;

	for (size_t i = 0; i < n && '\0' != *str; i++) {
		if (0xf0 == (0xf8 & *str)) {
			// 4-byte utf8 code point (began with 0b11110xxx)
			str += 4;
			length += 4;
		} else if (0xe0 == (0xf0 & *str)) {
			// 3-byte utf8 code point (began with 0b1110xxxx)
			str += 3;
			length += 3;
		} else if (0xc0 == (0xe0 & *str)) {
			// 2-byte utf8 code point (began with 0b110xxxxx)
			str += 2;
			length += 2;
		} else { // if (0x00 == (0x80 & *s)) {
			// 1-byte ascii (began with 0b0xxxxxxx)
			str += 1;
			length += 1;
		}
	}
	return length;
}

static void print_scrolled(const int trun_size, const int offset,
			   const char *prefix, const char *affix,
			   const char *separator, char *text)
{
	utf8cat(text, separator);

	int truncated_offset = offset % utf8len(text);
	int byte_offset =
		truncated_offset == 0 ? 0 : utf8chrlen(text, truncated_offset);
	char *text_ptr = text + byte_offset;
	char output[MAX_LEN];

	{
		char *output_ptr = output;
		int codepoint;
		void *v = utf8codepoint(text_ptr, &codepoint);
		for (int i = 0; i < trun_size;
		     v = utf8codepoint(v, &codepoint), i++) {
			if ('\0' == codepoint) {
				v = utf8codepoint(text, &codepoint);
			};

			output_ptr = utf8catcodepoint(output_ptr, codepoint,
						      MAX_LEN);
		}
	}
	printf("%s%s%s\n", prefix, output, affix);
}

static void print_scrolled_full_width(const int trun_size, const int offset,
				      const char *prefix, const char *affix,
				      const char *separator, char *text)
{
	utf8cat(text, separator);

	int truncated_offset = offset % utf8len(text);
	int byte_offset =
		truncated_offset == 0 ? 0 : utf8chrlen(text, truncated_offset);
	char *text_ptr = text + byte_offset;
	char output[MAX_LEN];

	{
		char *output_ptr = output;
		int codepoint;
		void *v = utf8codepoint(text_ptr, &codepoint);
		for (int i = 0; i < trun_size;
		     v = utf8codepoint(v, &codepoint), i++) {
			if ('\0' == codepoint) {
				v = utf8codepoint(text, &codepoint);
			};

			output_ptr = utf8catcodepoint(output_ptr, codepoint,
						      MAX_LEN);

			int w = mk_wcwidth((wchar_t)codepoint);
			if (w == 2) {
				i++;
			}

			if (i == trun_size - 1) {
				output_ptr = utf8catcodepoint(output_ptr, 32,
							      MAX_LEN);
			}
		}
	}
	printf("%s%s%s\n", prefix, output, affix);
}

int main(int argc, char *argv[])
{
	int truncate_size = 0;
	int offset = 0;
	char *prefix = "";
	char *affix = "";
	char *separator = "";
	char text[MAX_LEN];
	int input_method = 1;
	char handle_full_width = 0;

	for (int i = 1; i < argc; i++) {
		if (!strncmp(argv[i], "-", 2)) {
			input_method = 1;
		} else if (!strncmp(argv[i], "-t", 2))
			truncate_size = atoi(argv[++i]);
		else if (!strncmp(argv[i], "-o", 2))
			offset = atoi(argv[++i]);
		else if (!strncmp(argv[i], "-p", 2))
			prefix = argv[++i];
		else if (!strncmp(argv[i], "-a", 2))
			affix = argv[++i];
		else if (!strncmp(argv[i], "-f", 2))
			handle_full_width = 1;
		else if (!strncmp(argv[i], "-s", 2))
			separator = argv[++i];
		else if (!strncmp(argv[i], "-h", 2)) {
			usage();
			return EXIT_SUCCESS;
		} else {
			usage();
			return EXIT_FAILURE;
		}
	}
	read(STDIN_FILENO, text, MAX_LEN);

	if (input_method == 0) {
		printf("Input not provided!\n");
		return EXIT_FAILURE;
	}

	if (handle_full_width)
		print_scrolled_full_width(truncate_size, offset, prefix, affix,
					  separator, text);
	else
		print_scrolled(truncate_size, offset, prefix, affix, separator,
			       text);

	return EXIT_SUCCESS;
}
