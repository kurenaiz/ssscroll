# ssscroll
sakura's simple scroll is a simple text scroll script written in c99. It reads from stdin, transforms the text and outputs to stdout.

# Make dependencies
make, musl (by default, optional if you edit the Makefile)

# Usage & Examples

- `-t number` (REQUIRED)

  truncate text to `number`, the text is wrapped if `number` exceeds the length of the input

```shell
$ printf "123456" | ssscroll -t 3 -
# 123

$ printf "123456" | ssscroll -t 10 -
# 1234561234
```

- `-o number`

  offsets the start of the string by `number`

```shell
$ printf "123456" | ssscroll -t 3 -
# 123
$ printf "123456" | ssscroll -t 3 -o 2 -
# 345
```

- `-a string`

  affix

```shell
$ printf "123456" | ssscroll -t 3 -o 2 -a "<<<" -
# 345<<<
```

- `-p string`

  prefix

```shell
$ printf "123456" | ssscroll -t 3 -o 2 -p ">>>" -
# >>>345
```

- `-s string`

  separator

```shell
$ printf "123456" | ssscroll -t 10 -o 2 -s "~~~" -
# 3456~~~123
```

- `-f`

  makes ssscroll treat full width characters as two cells, this "stabilizes" the width of the text


# Examples

```shell
$ printf "μ's - 始まりの朝 (メインテーマ)" | ssscroll -t 10 -
# μ's - 始まりの

$ printf "μ's - 始まりの朝 (メインテーマ)" | ssscroll -t 10 -o 4 -
# - 始まりの朝 (メ

$ printf "μ's - 始まりの朝 (メインテーマ)" | ssscroll -t 10 -o 4 -p "「「 " -a " 」」" -
#「「 - 始まりの朝 (メ 」」

$ printf "μ's - 始まりの朝 (メインテーマ)" | ssscroll -t 10 -o 18  -s " 。。。" -
# マ) 。。。μ's

$ for i in $(seq 0 10); do printf "μ's - 始まりの朝 (メインテーマ)" | ssscroll -t 10 -a "|" -p ">" -o $i -; done
# >μ's - 始まりの|
# >'s - 始まりの朝|
# >s - 始まりの朝 |
# > - 始まりの朝 (|
# >- 始まりの朝 (メ|
# > 始まりの朝 (メイ|
# >始まりの朝 (メイン|
# >まりの朝 (メインテ|
# >りの朝 (メインテー|
# >の朝 (メインテーマ|
# >朝 (メインテーマ)|

# -f flags makes ssscroll treat full width characters as two cells, this "stabilizes" the width of the text
$ for i in $(seq 0 10); do printf "μ's - 始まりの朝 (メインテーマ)" | ssscroll -t 10 -a "|" -p ">" -f -o $i -; done
# >μ's - 始ま |
# >'s - 始まり|
# >s - 始まり |
# > - 始まりの|
# >- 始まりの |
# > 始まりの朝|
# >始まりの朝 |
# >まりの朝 ( |
# >りの朝 (メ |
# >の朝 (メイ |
# >朝 (メイン |
```

