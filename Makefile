# sssscroll

.PHONY: all clean install uninstall

CC = musl-gcc
PROJ_NAME = ssscroll
PREFIX = /usr/local
OUT_DIR = .

SRC :=  ssscroll.c \
	wcwidth.c
OBJ := $(SRC:%.c=$(OUT_DIR)/%.o)

# RELEASE or DEBUG
BUILD_MODE ?= RELEASE

# -L linker flags
LDFLAGS := -L/usr/lib/musl/lib/ -Wl,--gc-sections
# -l lib flags
LDLIBS   := -static /usr/lib/musl/lib/libc.a
CFLAGS := -Wall -std=c99 -Wno-missing-braces -Werror=pointer-arith -fPIE
CPPFLAGS := -I.

EXE = $(OUT_DIR)/$(PROJ_NAME)

ifeq ($(BUILD_MODE),DEBUG)
	#  -g				include debug information on compilation
	#  additional warnings
	#  -MD				generate dependency files
	CFLAGS += -D_DEBUG -g -Wextra -Wpedantic -Wformat=2 -Wno-unused-parameter -Wshadow -Wwrite-strings -Wstrict-prototypes -Wold-style-definition -Wredundant-decls -Wnested-externs -Wmissing-include-dirs
endif
ifeq ($(BUILD_MODE),RELEASE)
	#  -O2                      defines optimization level
	#  -s                       strip unnecessary symbols from build
	CFLAGS += -s -O2 -fdata-sections -ffunction-sections
endif

all: $(EXE)

$(EXE): $(OBJ)
	$(CC) $(LDFLAGS) $^ $(LDLIBS) -o $@

%.o: %.c
	$(CC) $(CPPFLAGS) $(CFLAGS) -c $< -o $@

clean:
	rm -f $(EXE) *.o *.d

install: all
	@echo "INSTALL bin/$(PROJ_NAME)"
	mkdir -p $(DESTDIR)/$(PREFIX)/bin
	cp $(OUT_DIR)/$(PROJ_NAME) $(DESTDIR)$(PREFIX)/bin/
	chmod 755 $(DESTDIR)/$(PREFIX)/bin/$(PROJ_NAME)

uninstall:
	@echo "REMOVE bin/$(PROJ_NAME)"
	rm -f $(DESTDIR)$(PREFIX)/bin/$(PROJ_NAME)
